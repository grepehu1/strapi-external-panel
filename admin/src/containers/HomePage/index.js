/*
 *
 * HomePage
 *
 */

import React, { memo } from 'react';
import { useState } from 'react';
import Panel from './components/Panel';
// import PropTypes from 'prop-types';
import useAuth from './useAuth';

const HomePage = () => {
  const [user, setUser] = useState(null)
  const [password, setPassword] = useState(null)
  const [submit, setSubmit] = useState(null)
  const jwt = useAuth(user,password,submit)

  function login(event){
    event.preventDefault()
    setSubmit(true)
  }

  return (
    <div className="labelsArc">
      {!jwt && <div>
        <div className="authHeader">
          <h1>Authenticate</h1>
          <h4>Use an authenticated user</h4>
        </div>

        <form onSubmit={login}>
          <label htmlFor="user">Authenticated User</label>
          <input type="text" className="form-control" id="user" name="user" onChange={chg => setUser(chg.target.value)}/>
          <label htmlFor="password">Password</label>
          <input type="password" className="form-control" id="password" name="password" onChange={chg => setPassword(chg.target.value)}/>
          <button type="submit" id="login-submit" className="btn btn-primary labelsBtn">Login</button>
        </form>
      </div>}
      {jwt && <Panel jwt={jwt}/>}

    </div>
  );
};

export default memo(HomePage);
