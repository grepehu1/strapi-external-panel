import React from 'react'
import { useState } from 'react' 
import ArticlesPanel from './ArticlesPanel'
import useCreateArticles from './useCreateArticles'

export default function CreateArticle({jwt}) {
    const [title, setTitle] = useState(null)
    const [body, setBody] = useState(null)
    const [submit, setSubmit] = useState(null)
    const goBack = useCreateArticles(jwt,title,body,submit)
    
    function submitNewForm(event){
        event.preventDefault()
        setSubmit(true)
    }

    return (
        <div>
            {goBack && <ArticlesPanel jwt={jwt} />}
            {!goBack && <a onClick={() => setGoBack(true)}>Back to Articles</a>}
            {!goBack && jwt && <form onSubmit={submitNewForm}>
                <div>
                    <label htmlFor="titleArticle">Title</label>
                    <input 
                    type="text" 
                    className="form-control"
                    name="titleArticle" 
                    id="titleArticle" 
                    onChange={(chg) => setTitle(chg.target.value)}>
                    </input>
                </div>
                <div>
                    <label htmlFor="titleBody">Body</label>
                    <input 
                    type="text" 
                    className="form-control"
                    name="titleBody" 
                    id="titleBody" 
                    onChange={(chg) => setBody(chg.target.value)}>
                    </input>
                </div>
                <button type="submit">Create</button>
            </form>}
        </div>
    )
}
