import React from 'react'
import { useState , useEffect} from 'react'

export default function useDeleteArticle(jwt, id=null) {
    const [refresh,setRefresh] = useState(null)
    let url = "http://localhost:1337/articles"

    if(id){
       url = url + `/${id}` 
    }

    useEffect(()=>{
        
        if (id) {
            fetch(url,{
                method: "DELETE",
                headers: {
                    "Content-Type" : "application/json",
                    "Authorization": `Bearer ${jwt}`
                  }
            }).then(res => res.json()).then(data => setRefresh(true))
        }
        
    }, [id])

    return refresh
}
