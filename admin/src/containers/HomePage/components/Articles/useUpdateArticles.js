import React from 'react'
import { useState , useEffect} from 'react'

export default function useUpdateArticles(jwt, id, title, body, submit) {
    const [goBack,setGoBack] = useState(null)
    let url = "http://localhost:1337/articles"

    if(id){
       url = url + `/${id}` 
    }

    useEffect(()=>{
        
        if (submit) {
            fetch(url,{
                method: "PUT",
                headers: {
                    "Content-Type" : "application/json",
                    "Authorization": `Bearer ${jwt}`
                  },
                body: JSON.stringify({
                    "Title": title,
                    "Body": body
                })
            }).then(res => res.json()).then(data => setGoBack(true))
        }
        
    }, [submit])

    
    return goBack
}
