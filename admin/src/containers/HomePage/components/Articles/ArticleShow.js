import React from 'react'
import ArticlesPanel from './ArticlesPanel'
import useFetchArticles from './useFetchArticles'
import { useState } from 'react'

export default function ArticleShow({jwt,id}) {
    const article = useFetchArticles(jwt,id)
    const [goBack,setGoBack] = useState(null)

    return (
        <div>
            {goBack && <ArticlesPanel jwt={jwt} />}
            {!goBack && <a onClick={() => setGoBack(true)}>Back to List</a>}
            {!goBack && article && <div>
                <h3>{article.Title}</h3>
                <p>{article.Body}</p>
                <p>{formatDate(article.published_at)}</p>
            </div>}
        </div>
    )
}

function formatDate(date){
    const dateNew = new Date(date)

    return dateNew.toDateString()
}