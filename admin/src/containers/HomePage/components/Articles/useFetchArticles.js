import React from 'react'
import { useState , useEffect} from 'react'

export default function useFetchArticles(jwt,id=null) {
    const [articles, setArticles] = useState(null)
    let url = "http://localhost:1337/articles"

    if(id){
       url = url + `/${id}` 
    }

    useEffect(()=>{
        fetch(url,{
            method: "GET",
            headers: {
                "Content-Type" : "text/plain",
                "Authorization": `Bearer ${jwt}`
              }
        }).then(res => res.json()).then(data => setArticles(data))
    }, [])

    
    return articles
}
