import React from 'react'
import useFetchArticles from './useFetchArticles'
import { useState } from 'react'
import ArticleShow from './ArticleShow'
import Panel from '../Panel'
import CreateArticle from './CreateArticle'
import useDeleteArticle from './useDeleteArticle'
import UpdateArticle from './UpdateArticle'

export default function ArticlesPanel({jwt}) {
    const [id, setId] = useState(false)
    const [delId, setDelId] = useState(false)
    const [updArt, setUpdArt] = useState(false)
    const [goBack,setGoBack] = useState(null)
    const [create, setCreate] = useState(null)
    const articles = useFetchArticles(jwt)
    const refresh = useDeleteArticle(jwt,delId)

    return (
        <div>
            {goBack && <Panel jwt={jwt} />}
            {!updArt && !create && !goBack && !id && <a onClick={() => setGoBack(true)}>Back to Panel</a>}
            {!updArt && !create && !goBack && !id && articles && articles.map(art => {
                return (
                    <div key={art.id}>
                        <div onClick={() => setId(art.id)}>
                            <h3>{art.Title}</h3>
                            <p>{art.Body.slice(0,20) + '...'}</p>
                            <p>{formatDate(art.published_at)}</p>
                        </div>
                        <button 
                        className="btn btn-warning"
                        onClick={() => setUpdArt(art)}>Update</button>
                        <button 
                        className="btn btn-danger"
                        onClick={() => setDelId(art.id)}>Delete</button>
                    </div>
                )
            })}
            {!updArt && !create && !goBack && !id && <button className="btn btn-primary" onClick={() => setCreate(true)}>Create New Article</button>}
            {!updArt && !create && !goBack && id && <ArticleShow jwt={jwt} id={id}/>}
            {!updArt && create && !goBack && <CreateArticle jwt={jwt}/>}
            {updArt && !create && !goBack && <UpdateArticle jwt={jwt} article={updArt}/>}
        </div>
    )
}

function formatDate(date){
    const dateNew = new Date(date)

    return dateNew.toDateString()
}
