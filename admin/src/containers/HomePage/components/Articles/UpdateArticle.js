import React from 'react'
import { useState } from 'react' 
import ArticlesPanel from './ArticlesPanel'
import useUpdateArticles from './useUpdateArticles'

export default function UpdateArticle({jwt,article}) {
    const [title, setTitle] = useState(article.Title)
    const [body, setBody] = useState(article.Body)
    const [submit, setSubmit] = useState(null)
    const goBack = useUpdateArticles(jwt,article.id,title,body,submit)
    
    function submitNewForm(event){
        event.preventDefault()
        setSubmit(true)
    }

    return (
        <div>
            {goBack && <ArticlesPanel jwt={jwt} />}
            {!goBack && <a onClick={() => setGoBack(true)}>Back to Articles</a>}
            {!goBack && jwt && <form onSubmit={submitNewForm}>
                <div>
                    <label htmlFor="titleArticle">Title</label>
                    <input 
                    type="text" 
                    className="form-control"
                    name="titleArticle" 
                    id="titleArticle" 
                    onChange={(chg) => setTitle(chg.target.value)}
                    value={title}>
                    </input>
                </div>
                <div>
                    <label htmlFor="titleBody">Body</label>
                    <input 
                    type="text" 
                    className="form-control"
                    name="titleBody" 
                    id="titleBody" 
                    onChange={(chg) => setBody(chg.target.value)}
                    value={body}>
                    </input>
                </div>
                <button type="submit">Update</button>
            </form>}
        </div>
    )
}
