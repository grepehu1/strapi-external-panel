import React from 'react'
import { useState , useEffect} from 'react'

export default function useCreateArticles(jwt, title, body, submit) {
    let url = "http://localhost:1337/articles"
    const [goBack,setGoBack] = useState(null)

    useEffect(()=>{
        
        if (submit) {
            fetch(url,{
                method: "POST",
                headers: {
                    "Content-Type" : "application/json",
                    "Authorization": `Bearer ${jwt}`
                  },
                body: JSON.stringify({
                    "Title": title,
                    "Body": body
                })
            }).then(res => res.json()).then(data => setGoBack(true))
        }
        
    }, [submit])

    
    return goBack
}
