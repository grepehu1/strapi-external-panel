import React from 'react'
import { useState } from 'react'
import ArticlesPanel from './Articles/ArticlesPanel'

export default function Panel({jwt}) {
    const types = ['Articles','Writers']
    const [panel,setPanel] = useState(null)

    return (
        <div>
            {!panel && <h1>Existing Types of Content</h1>}
            <div>
                {!panel && types.map(typ => {
                    return (
                    <div key={typ}>
                        <button className="btn btn-primary" onClick={() => setPanel(typ)}>{typ}</button>
                    </div>
                    )
                })}
            </div>
            <div>
                <p>{panel && <ArticlesPanel jwt={jwt}/>}</p>
            </div>
        </div>
    )
}
